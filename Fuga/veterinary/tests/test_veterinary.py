from django.urls import reverse, resolve
from django.test import TestCase
from ..views import home, get_vet
from ..models import Veterinary
from model_mommy import mommy

# Create your tests here.
class HomeTests(TestCase):
    def test_home_view_status_code(self):
        url = reverse('home')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_home_url_resolves_home_view(self):
        pass
        """view = resolve('home')
        self.assertEquals(view.func, home)"""

class NewVetTests(TestCase):
    def setUp(self):
        Veterinary.objects.create(name='Kathurima', email='john@doe.com', county='Kiambu', id_no='123456', phone_number='0705385894')

    def test_csrf(self):
        """Fix test csrf"""
        """url = reverse('vet-create')
        response = self.client.get(url)
        self.assertContains(response, 'csrfmiddlewaretoken')"""

    def test_new_veterinary_valid_post_data(self):
        """To be fixed"""
        pass
        """url = reverse('vet-create')
        data = {
            'name': 'Test title',
            'email': 'jon@doe.com',
            'county': 'kiambu',
            'id_no': '123456',
            'phone_number': '123456789'
        }
        response = self.client.post(url, data)
        self.assertTrue(Veterinary.objects.exists())"""

    def test_new_vet_invalid_post_data(self):
        """Fix Later"""
        pass
        """'''
        Invalid Veterinary data should not redirect
        The expected behavior is to show the form again with validation errors
        '''
        url = reverse('vet-create')
        response = self.client.post(url, {})
        form = response.context.get('form')
        self.assertEquals(response.status_code, 200)
        self.assertTrue(form.errors)"""

    def test_new_vet_invalid_post_data_empty_fields(self):
        pass
        """'''
        Invalid Veterinary data should not redirect
        The expected behavior is to show the form again with validation errors
        '''
        url = reverse('vet-create')
        data = {
            'name': '',
            'email': '',
            'county': '',
            'id_no': '',
            'phone_number': ''
        }
        response = self.client.post(url, data)
        self.assertEquals(response.status_code, 200)
        self.assertTrue(Veterinary.objects.exists())"""
        
class VeterinaryTestMommy(TestCase):
    """Testing Vet creation"""
    def test_vet_creation(self):
        vet = mommy.make('veterinary.Veterinary')
        self.assertTrue(isinstance(vet, Veterinary))
        self.assertEqual(vet.__str__(), vet.name)

class GetVetTests(TestCase):
    def setUp(self):
        return Veterinary.objects.create(id_no=123456, name='Kathurima', email='john@doe.com', county='Kiambu',
                                  phone_number='0705385894')

    def test_vet_id_resolves_vet_view(self):
        """Test Vet ID No. resolves to the correct view"""
        view = resolve('/vet/23/')
        self.assertEquals(view.func, get_vet)