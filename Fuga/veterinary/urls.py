from django.conf.urls import url

from . import views as vet_views

urlpatterns = [
    url(r'^$', vet_views.home, name='home'),
    url('vet/create/', vet_views.VeterinaryCreate.as_view(), name='vet-create'),
    url(r'^vet/(?P<pk>\d+)/$', vet_views.get_vet, name='get-vet'),
    url('vet/(?P<pk>\d+)/update/', vet_views.VeterinaryUpdate.as_view(), name='vet-update'),
    url('vet/(?P<pk>\d+)/delete/', vet_views.VeterinaryDelete.as_view(), name='vet-delete'),
]
