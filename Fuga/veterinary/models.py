from django.db import models

# Create your models here.
class Veterinary(models.Model):
    """Farmer models class"""
    id_no = models.IntegerField(primary_key=True, unique=True)
    name = models.CharField(max_length=25)
    email = models.EmailField(max_length=100)
    county = models.CharField(max_length=25)
    phone_number = models.CharField(max_length=25)

    def __str__(self):
        return self.name