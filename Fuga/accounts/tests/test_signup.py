from django.contrib.auth.models import User
from django.urls import resolve, reverse
from django.test import TestCase
from ..views import register
from ..forms import RegisterForm

class SignUpTests(TestCase):
    def setUp(self):
        url = reverse('register')
        self.response = self.client.get(url)

    def test_signup_status_code(self):
        self.assertEquals(self.response.status_code, 200)

    def test_signup_url_resolves_signup_view(self):
        pass
        """view = resolve('/register/')
        self.assertEquals(view.func, register)"""

    def test_csrf(self):
        """Test csrf"""
        self.assertContains(self.response, 'csrfmiddlewaretoken')

    def test_contains_form(self):
        """Test contains form"""
        form = self.response.context.get('form')
        self.assertIsInstance(form, RegisterForm)


class SuccessfulSignUpTests(TestCase):
    def setUp(self):
        url = reverse('register')
        data = {
            'username': 'john',
            'email': 'john@doe.com',
            'password1': 'abcdef123456',
            'password2': 'abcdef123456'
        }
        self.response = self.client.post(url, data)
        self.home_url = reverse('login')

    def test_redirection(self):
        """Valid form submission redirects ro homepage"""
        self.assertRedirects(self.response, self.home_url)

    def test_user_creation(self):
        """Test user existence"""
        self.assertTrue(User.objects.exists())

    def test_user_authentication(self):
        """Test user authentication"""
        response = self.client.get(self.home_url)
        user = response.context.get('user')
        self.assertTrue(user.is_authenticated)


class InvalidSignUpTests(TestCase):
    def setUp(self):
        url = reverse('register')
        self.response = self.client.post(url, {})  # submit an empty dictionary

    def test_signup_status_code(self):
        '''
        An invalid form submission should return to the same page
        '''
        self.assertEquals(self.response.status_code, 200)

    def test_form_errors(self):
        form = self.response.context.get('form')
        self.assertTrue(form.errors)

    def test_dont_create_user(self):
        self.assertFalse(User.objects.exists())


class SuccessfulSignUpTests(TestCase):
    def setUp(self):
        url = reverse('register')
        data = {
            'username': 'john',
            'email': 'john@doe.com',
            'password1': 'abcdef123456',
            'password2': 'abcdef123456'
        }
        self.response = self.client.post(url, data)
        self.home_url = reverse('login')

    def test_form_inputs(self):
        pass
        """
        Test form inputs in our HTML template

        The view must contain five inputs: csrf, username, email,
        password1, password2
        """
        """self.assertContains(self.response, '<input', 5)
        self.assertContains(self.response, 'type="text"', 1)
        self.assertContains(self.response, 'type="email"', 1)
        self.assertContains(self.response, 'type="password"', 2)"""
