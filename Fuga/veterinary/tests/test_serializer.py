from django.test import TestCase, RequestFactory, Client
from django.utils import timezone

from ..serializers import VeterinarySerializer
from ..models import Veterinary

import json
from rest_framework.request import Request
from rest_framework.reverse import reverse, reverse_lazy
from rest_framework.status import (HTTP_200_OK,
                                   HTTP_404_NOT_FOUND,
                                   HTTP_201_CREATED,
                                   HTTP_400_BAD_REQUEST,
                                   HTTP_204_NO_CONTENT)
from rest_framework.test import APIRequestFactory

# initialize the APIClient app
client = Client()

class VeterinarySerializerTest(TestCase):
    def create_order(self):
        return Veterinary.objects.create(id_no='123456', name='Kathurima', email='john@doe.com', county='Kiambu', phone_number='0705385894')

    def test_veterinary_serializer(self):
        """Test Veterinary Serializer"""
        request = APIRequestFactory().get("")
        context = {'request': Request(request)}
        order = self.create_order()
        order_serializer = VeterinarySerializer(order, context=context)

        data = {
            'id_no': order_serializer.data['id_no'],
            'name': order_serializer.data['name'],
            'email': order_serializer.data['email'],
            'county': order_serializer.data['county'],
            'phone_number': order_serializer.data['phone_number']
        }

        response = client.post(reverse('veterinary-list'),
                               data=json.dumps(data),
                               content_type='application/json')

        assert response.status_code == 302