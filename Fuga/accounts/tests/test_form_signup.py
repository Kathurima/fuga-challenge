from django.test import TestCase
from ..forms import RegisterForm

class SignUpFormTest(TestCase):
    """Test form has the required fields"""
    def test_form_has_fields(self):
        form = RegisterForm()
        expected = ['username', 'email', 'password1', 'password2',]
        actual = list(form.fields)
        self.assertSequenceEqual(expected, actual)