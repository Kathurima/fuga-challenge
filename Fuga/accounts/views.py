from django.shortcuts import render, redirect
from django.contrib.auth import login
from django.contrib import messages
from .forms import RegisterForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import UpdateView

# Create your views here.
def register(request):
	if request.method == 'POST':
		form = RegisterForm(request.POST)
		# Checking for form validity
		if form.is_valid():
			user = form.save()
			# Use flash message to show reception of valid data
			messages.success(request, f'Account created successfully! You can now login')
			login(request, user)
			return redirect('login')
	else:
		form = RegisterForm()

	return render(request, 'accounts/register.html', {'form': form})

@method_decorator(login_required, name='dispatch')
class UserUpdateView(UpdateView):
    model = User
    fields = ('email',)
    template_name = 'accounts/my_account.html'
    success_url = reverse_lazy('home')

    def get_object(self):
        return self.request.user