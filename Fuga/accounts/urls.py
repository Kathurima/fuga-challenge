from django.conf.urls import url
from django.contrib.auth import views as auth_views

from accounts import views as account_views

urlpatterns = [
    url(r'^login/$', auth_views.LoginView.as_view(template_name='accounts/login.html'), name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),
    url(r'^register/$', account_views.register, name='register'),
    url(r'^settings/account/$', account_views.UserUpdateView.as_view(), name='my_account'),
]
