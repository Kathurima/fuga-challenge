from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import render, get_object_or_404
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Veterinary

from rest_framework import status, viewsets
from .serializers import VeterinarySerializer

# Create your views here.
def home(request):
    vets = Veterinary.objects.all()
    return render(request, 'veterinary/home.html', {'vets': vets})

@login_required
def get_vet(request, pk):
    vet = get_object_or_404(Veterinary, pk=pk)

    return render(request, 'veterinary/veterinary.html', {'vet': vet})

class VeterinaryCreate(LoginRequiredMixin, CreateView):
    model = Veterinary
    fields = ['id_no', 'name', 'email', 'county', 'phone_number']
    success_url = reverse_lazy('home')

class VeterinaryUpdate(LoginRequiredMixin, UpdateView):
    model = Veterinary
    context_object_name = 'vetupdate'
    fields = ['county', 'phone_number']
    success_url = reverse_lazy('home')

class VeterinaryDelete(LoginRequiredMixin, DeleteView):
    model = Veterinary
    success_url = reverse_lazy('home')

# REST Views
class VeterinaryViewSet(LoginRequiredMixin, viewsets.ModelViewSet):
    queryset = Veterinary.objects.all()
    serializer_class = VeterinarySerializer
