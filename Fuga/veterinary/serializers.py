from rest_framework import serializers

from .models import Veterinary

class VeterinarySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Veterinary
        fields = ['id_no', 'name', 'email', 'county', 'phone_number']