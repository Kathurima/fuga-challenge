# Fuga Challenge
Backend challenge code

This project has six tasks:
- Implement Logging in
- List veterinary officers
- Update a veterinary officer information
- Deleting a veterinary officer
- Create APIs with the same functionality as listed above

## Technology Stack
- Python
- Django
- Django Rest Framework
- Heroku
- Whitenoise
- Postgresql
- Coverage

## Basic Commands
### Test coverage
First, navigate to project's root directory
`cd Fuga`


To run the tests, check your test coverage, and generate a simplified coverage report:

`$ coverage run manage.py tests`

To generate an HTML report:

`coverage report`

`coverage html`

`open htmlcov/index.html`

## Running the project locally
First, clone the repository to your local machine:

`git@gitlab.com:fuga-challenge/fuga-challenge.git`

Install the requirements:

`pip3 install -r requirements.txt`

Create the database by running migrations:

`python3 manage.py migrate`

Run the development server:

`python3 manage.py runserver`

The project will be available at **127.0.0.1:8000**

## Deployment
This project was automatically deployed to Heroku via gitlab CI/CD jobs.
