from .models import Veterinary
from django import forms

class VeterinaryForm(forms.ModelForm):
    class Meta:
        model = Veterinary
        fields = ['id_no', 'name', 'email', 'county', 'phone_number']